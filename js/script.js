/*
get access token:
 https://oauth.vk.com/authorize?client_id=5239989&redirect_uri=https://oauth.vk.com/blank.html&display=page&scope=messages,offline&response_type=token&v=5.44

render emoji:
 https://habrahabr.ru/post/263455/

get dialogs:
 https://vk.com/dev/messages.getDialogs

get history:
 https://vk.com/dev/messages.getHistory

get users:
 https://vk.com/dev/users.get
 */

function loadJSONP(url, callback) {
    var ref = window.document.getElementsByTagName('script')[0];
    var script = window.document.createElement('script');
    script.src = url + ((url.match(/\?/)) ? '&' : '?') + "callback=" + callback;
    ref.parentNode.insertBefore(script, ref);
    script.onload = function () {
        this.remove();
    };
}

function convertTimestamp(timestamp) {
    var d = new Date (timestamp * 1000),
        yyyy = d.getFullYear(),
        mm = ('0' + (d.getMonth() + 1)).slice(-2),
        dd = ('0' + d.getDate()).slice(-2),
        hh = ('0' + d.getHours()).slice(-2),
        min = ('0' + d.getMinutes()).slice(-2),
        sec = ('0' + (d.getSeconds() + 1)).slice(-2),
        time;
    time = dd + '.' + mm + '.' + yyyy + ' ' + hh + ':' + min + ':' + sec;
    return time;
}

var connector = new Connector('connector');
var APP = null;

function init() {
    APP = new Application(document.getElementById('access_token').value, connector);
    APP.render.show();
    APP.updateDialogs();
}

function getHistory(id) {
    APP.updateHistory(id);
}

function saveHistoryInFile() {
    APP.render.inFile();
}

function captcha(http_request, captcha_img, captcha_sid, connection) {
    document.getElementById('captcha_img').src = captcha_img;
    document.getElementById('captcha_http_request').value = http_request;
    document.getElementById('captcha_sid').value = captcha_sid;
    document.getElementById('captcha_connection').value = connection;
    APP.render.showCaptcha();
}

function sendCaptcha() {
    APP.render.hideCaptcha();
    var http_request = document.getElementById('captcha_http_request').value;
    var connection = document.getElementById('captcha_connection').value;
    var sid = document.getElementById('captcha_sid').value;
    var key = document.getElementById('captcha_key').value;
    http_request += '&captcha_sid='+sid+'&captcha_key='+key;
    loadJSONP(http_request, connection);
}

function Connector(name) {
    this.name = name;
    this.methods = [];
    this.add = function(object, method, extra_data) {
        var current_connection = this.name+'.methods['+(this.methods.length)+']';
        this.methods.push(function (data) {
            if (data.error && data.error.error_code == 14) {
                captcha(extra_data.http_request, data.error.captcha_img, data.error.captcha_sid, current_connection);
            } else {
                object[method](data, extra_data);
            }

        });
        return current_connection;
    }
}

function Render(application) {
    var app = application;
    var rerender_dialogs = false;
    var rerender_history = false;
    var actual_history = null;

    this.show = function() {
        var popup_element = document.getElementsByClassName("popup_background")[0];
        var wrapper_element = document.getElementsByClassName("wrapper")[0];
        popup_element.style.visibility = 'hidden';
        popup_element.style.display = 'none';
        wrapper_element.style.display = 'block';
        wrapper_element.style.visibility = 'visible';
    };

    this.showCaptcha = function() {
        var captcha_dialog = document.getElementById("captcha_dialog");
        captcha_dialog.style.display = 'block';
        captcha_dialog.style.visibility = 'visible';
    };

    this.hideCaptcha = function() {
        var captcha_dialog = document.getElementById("captcha_dialog");
        captcha_dialog.style.visibility = 'hidden';
        captcha_dialog.style.display = 'none';
    };

    function reselectDialog() {
        if (document.getElementsByClassName('selected')[0]) {
            document.getElementsByClassName('selected')[0].classList.remove('selected');
        }
        if (actual_history) {
            document.getElementById('dialog_' + actual_history).classList.add('selected');
        }
    }

    this.setActualDialog = function(id) {
        actual_history = id;
        rerender_dialogs = true;
        rerender_history = true;
        document.getElementById('history').innerHTML = '';
        this.disableSaveButton();
        reselectDialog();
    };

    this.setDialogToRender = function() {
        this.disableSaveButton();
        rerender_history = true;
    };

    this.updateDialogs = function() {
        document.getElementById('dialogs').innerHTML = app.dialogs.getHtml();
        reselectDialog();
        rerender_dialogs = false;
    };

    this.updateHistory = function() {
        document.getElementById('history').innerHTML = app.dialogs.getDialog(actual_history).getHistoryHtml();
        this.enableSaveButton();
        rerender_history = false;
    };

    this.update = function() {
        document.getElementById('save_btn').disabled = true;
        if (actual_history != null) {
            this.updateHistory();
        } else {
            this.updateDialogs();
        }
    };

    this.updateProgress = function(offset, count) {
        var w = (100*offset)/count;
        w += '%';
        document.getElementsByClassName('progress')[0].style.width = w;
    };

    this.enableSaveButton = function() {
        this.updateProgress(100, 100);
        document.getElementById('save_btn').disabled = false;
    };

    this.disableSaveButton = function() {
        document.getElementById('save_btn').disabled = true;
        document.getElementsByClassName('progress')[0].style.width = 0;
    };

    this.inFile = function() {
        var html = ''+
            '<style>'+
                'body { background: rgb(240, 242, 245); font-family: Tahoma, sans-serif; font-size: 10pt; }'+
                '.history { background: #fff; width: 96%; margin: auto;}'+
                '.row { display: flex; width: 100%; min-height: 50px; padding: 5px; }'+
                '.avatar-block { flex-basis: 50px; flex-shrink: 0; }'+
                '.avatar { width: 50px; height: 50px; }'+
                '.message-container { display: flex; flex-direction: column; flex-wrap: nowrap; flex-basis: 300px; flex-grow: 1; flex-shrink: 0; }'+
                '.message-container > div { padding: 3px; }'+
                '.name { color: rgb(43, 88, 122); font-weight: bold; }'+
                '.message { background: #fff; }'+
                '.time { color: #888; font-size: 8pt; padding: 3px; margin-left: 10px; flex-basis: 60px; flex-shrink: 0; }'+
                '.forwarded-message {width: 100%; margin: 5px; border-left: 3px solid rgb(195, 209, 224); padding: 5px; display: flex; flex-direction: row; flex-wrap: nowrap; }'+
                '.forwarded-wrapper { width: 100%; }'+
                '.forwarded-message > div { margin: 0 5px 0 0; }'+
                '.forwarded-message .avatar-block { flex-basis: 30px; flex-shrink: 0; }'+
                '.forwarded-message .avatar { width: 30px; height: 30px; }'+
                '.forwarded-message .user-time-block { margin: 5px 5px; display: flex; flex-direction: row; }'+
                '.forwarded-message .user-time-block > div { margin: 0 5px; }'+
                '.forwarded-message .time { flex-basis: 120px; }'+
            '</style>'+
            '<div class="history" id="history">'+app.dialogs.getDialog(actual_history).getHistoryHtml()+'</div>';
        var blob = new Blob([html], {type: "text/html;charset=utf-8"});
        saveAs(blob, 'vk_dialog_'+actual_history+'.html');
    };
}


function Application(access_token, connector) {
    this.access_token = access_token;
    this.connector = connector;
    this.dialogs = new Dialogs(this);
    this.users = new Users(this);
    this.render = new Render(this);
    this.timeout = 200;

    this.updateDialogs = function() {
        this.render.setDialogToRender();
        this.dialogs.getVkData();
    };

    this.updateHistory = function(id) {
        this.render.setActualDialog(id);
        this.dialogs.getVkHistoryData(id);
    };
}

function Dialogs(app) {
    this.app = app;
    this.count = 0;
    this.offset = 0;

    this.dialogs = {};
    this.order = [];

    this.extra_data = {http_request: ''};

    this.getVkData = function() {
        this.extra_data.http_request = 'https://api.vk.com/method/messages.getDialogs?offset='+this.offset+'&count=200&access_token=' + this.app.access_token;
        var request = this.extra_data.http_request;
        var con = this.connection;
        setTimeout(function() {
            loadJSONP(request, con);
        }, this.app.timeout);
    };

    this.getVkHistoryData = function(id) {
        this.dialogs[id].getVkData();
    };

    this.save = function(data) {
        if (data.error) {
            this.getVkData();
        } else {
            this.count = data.response[0];
            for (var i in data.response) {
                if (i != 0) {
                    var dialog_id = (data.response[i].chat_id) ? data.response[i].chat_id + 2000000000 : data.response[i].uid;
                    this.add(new Dialog(this.app, data.response[i]), dialog_id);
                    this.offset++;
                }
            }
            if (this.offset < this.count) {
                this.getVkData();
            } else {
                this.app.users.getVkData();
            }
        }
    };

    this.add = function(dialog, id) {
        this.dialogs[id] = dialog;
        this.dialogs[id].user = this.app.users.add(id);
        this.order.push({dialog_id: id, date: this.dialogs[id].date});
    };

    this.getDialog = function(id) {
        return this.dialogs[id];
    };

    this.getHtml = function() {
        var html = '';
        for (var i=0; i<this.order.length; i++ ) {
             html += this.dialogs[this.order[i].dialog_id].getHtml();
        }
        return html;
    };

    this.sort = function() {
        for (var i=0; i<this.order.length-1; i++) {
            for (var j=i; j<this.order.length; j++) {
                if (this.order[i].date < this.order[j].date) {
                    var temp = this.order[i];
                    this.order[i] = this.order[j];
                    this.order[j] = temp;
                }
            }
        }
    };

    this.connection = this.app.connector.add(this, 'save', this.extra_data);
}

function Users(app) {
    this.app = app;
    this.connection = null;
    this.count = 0;
    this.offset = 0;

    this.extra_data = {http_request: ''};

    this.users = {};

    this.getVkData = function() {
        if (this.count != 0 && this.offset < this.count) {
            var users_str = '';
            var request_offset = 0;
            for (var key in this.users) {
                if (request_offset < 200) {
                    if (this.users[key].name == '') {
                        users_str += key + ',';
                        request_offset++;
                    }
                }
            }
            users_str = users_str.substring(0, users_str.length - 1);
            this.extra_data.http_request = 'https://api.vk.com/method/users.get?user_ids=' + users_str + '&fields=photo_50&access_token=' + this.app.access_token;
            var request = this.extra_data.http_request;
            var con = this.connection;
            setTimeout(function() {
                loadJSONP(request, con);
            }, this.app.timeout);
        } else {
            this.app.render.update();
        }
    };

    this.save = function(data) {
        if (data.error) {
            this.getVkData();
        } else {
            for (var k in data.response) {
                this.users[data.response[k].uid].initialize(data.response[k]);
                this.offset++;
            }
            if (this.offset < this.count) {
                this.getVkData();
            } else {
                this.app.render.update();
            }
        }
    };

    this.add = function(id) {
        if (this.users == undefined || this.users[id] == undefined) {
            this.users[id] = new User(id);
            this.count++;
        }
        return this.users[id];
    };

    this.connection = this.app.connector.add(this, 'save', this.extra_data);
}

function User(user_id) {
    this.id = user_id;
    this.name = '';
    this.photo = '';

    this.initialize = function(response) {
        this.name = response.first_name + ' ' + response.last_name;
        this.photo = response.photo_50;
    }
}

function Dialog(app, response) {
    this.app = app;
    this.connection = null;
    this.id = (response.chat_id) ? response.chat_id + 2000000000 : response.uid;
    this.user = this.app.users.add((response.chat_id) ? response.admin_id : response.uid);
    this.photo = 'http://cs623219.vk.me/v623219550/3acdf/axZ70uMf9kM.jpg';
    this.title = Emoji.emojiToHTML(response.title);
    this.date = response.date;
    this.count = 0;
    this.offset = 0;
    this.messages = [];

    this.extra_data = {http_request: ''};

    this.getVkData = function() {
        this.extra_data.http_request = 'https://api.vk.com/method/messages.getHistory?user_id='+this.id+'&offset='+this.offset+'&count=200&access_token=' + this.app.access_token;
        var request = this.extra_data.http_request;
        var con = this.connection;
        setTimeout(function() {
            loadJSONP(request, con);
        }, this.app.timeout);
    };

    this.save = function(data) {
        if (data.error) {
            this.getVkData();
        } else {
            for (var i in data.response) {
                if (i == 0) {
                    this.count = data.response[0];
                } else {
                    this.messages.push(new Message(this.app, data.response[i]));
                    this.offset++;
                }
            }
            this.app.render.updateProgress(this.offset, this.count);
            if (this.offset < this.count) {
                this.getVkData();
            } else {
                this.app.users.getVkData();
            }
        }
    };

    this.getHtml = function() {
        var photo = '';
        var name = '';
        if (this.id > 2000000000) {
            photo = this.photo;
            name = this.title;
        } else {
            photo = this.user.photo;
            name = this.user.name;
        }
        var html = ''+
            '<div class="dialog" id="dialog_'+this.id+'" data-dialog="'+this.id+'" onclick="getHistory('+this.id+');">'+
            '<div class="avatar-block">'+
            '<img class="avatar" src="'+photo+'">'+
            '</div>'+
            '<div class="name">'+name+'</div>'+
            '</div>';
        return html;
    };

    this.getHistoryHtml = function() {
        var html = '';
        for (var i in this.messages) {
            html += this.messages[i].getHtml();
        }
        return html;
    };

    this.connection = this.app.connector.add(this, 'save', this.extra_data);
}

function Message(app, response) {
    this.app = app;
    this.user = this.app.users.add(response.uid);
    this.body = '';
    this.date = response.date;
    this.geo_html = '';
    this.attachments = new Attachments(response.attachments);
    this.messages = [];

    if (response.geo) {
        this.geo_html = '<a href="https://yandex.ru/maps/?ll='+response.geo.coordinates.split(" " )[1]+","+response.geo.coordinates.split(" ")[0]+'&z=17" target="_blank">'+response.geo.place.title+'</a>';
    }

    if (response.emoji == 1) {
        this.body = Emoji.emojiToHTML(response.body);
    } else {
        this.body = response.body;
    }

    if (response.fwd_messages) {
        for (var key in response.fwd_messages) {
            this.messages.push(new Message(this.app, response.fwd_messages[key]));
        }
    }


    this.getHtml = function() {
        var html = ''+
            '<div class="row">'+
                '<div class="avatar-block"><img class="avatar" src="'+this.user.photo+'"></div>'+
                '<div class="message-container">'+
                    '<div class="name">'+this.user.name+'</div>'+
                    '<div class="message">'+
                        '<div>'+this.body+'</div>'+
                        '<div>'+this.geo_html+'</div>'+
                        '<div>'+this.attachments.getHtml()+'</div>'+
                        '<div>'+this.getHtmlForwarded()+'</div>'+
                    '</div>'+
                '</div>'+
                '<div class="time">'+convertTimestamp(this.date)+'</div>'+
            '</div>';
        return html;
    };

    this.getHtmlForwarded = function() {
        var html = '';
        for (var key in this.messages) {
            html += this.messages[key].getHtmlForwardedMessage();
        }
        return html;
    };

    this.getHtmlForwardedMessage = function() {
        var html = ''+
            '<div class="forwarded-message">'+
                '<div class="avatar-block">'+
                    '<img class="avatar" src="'+this.user.photo+'">'+
                '</div>'+
                '<div class="forwarded-wrapper">'+
                    '<div class="user-time-block">'+
                        '<div class="name">'+
                            this.user.name+
                        '</div>'+
                        '<div class="time">'+
                            convertTimestamp(this.date)+
                        '</div>'+
                    '</div>'+
                    '<div class="message">'+
                        '<div>'+this.body+'</div>'+
                        '<div>'+this.geo_html+'</div>'+
                        '<div>'+this.attachments.getHtml()+'</div>'+
                        '<div>'+this.getHtmlForwarded()+'</div>'+
                    '</div>'+
                '</div>'+
            '</div>';
        return html;
    }
}

function Attachments(response) {
    var attachments = [];
    if (response) {
        for (var key in response) {
            addAttachment(response[key]);
        }
    }
    this.getHtml = function() {
        var html = '';
        if (attachments.length > 0) {
            for (var i = 0; i < attachments.length; i++) {
                html += attachments[i].getHtml();
            }
        }
        return html;
    };

    function addAttachment(attachment) {
        switch (attachment.type) {
            case 'video':
                attachments.push(new Video(attachment.video));
                break;
            case 'audio':
                attachments.push(new Audio(attachment.audio));
                break;
            case 'doc':
                attachments.push(new Doc(attachment.doc));
                break;
            case 'wall':
                attachments.push(new Wall(attachment.wall));
                break;
            case 'photo':
                attachments.push(new Photo(attachment.photo));
                break;
            case 'sticker':
                attachments.push(new Sticker(attachment.sticker));
                break;
        }
    }
}

function Video(response) {
    this.type = 'video';
    var owner_id = response.owner_id;
    var vid = response.vid;
    var access_key = response.access_key;
    var image = response.image;
    this.getHtml = function() {
        var html = '<div><a href="https://vk.com/video'+owner_id+'_'+vid+'" target="_blank"><img src="'+image+'"></a></div>';
        return html;
    }
}

function Audio(response) {
    var title = response.title;
    var artist = response.artist;
    this.type = 'audio';
    this.getHtml = function() {
        var html = 'Аудиозапись: '+artist+' - '+title;
        return html;
    }
}

function Photo(response) {
    var src = response.src;
    this.type = 'photo';
    this.getHtml = function() {
        var html = '<img src="'+src+'" width="300">';
        return html;
    }
}

function Sticker(response) {
    var photo = response.photo_128;
    this.type = 'sticker';
    this.getHtml = function() {
        var html = '<img src="'+photo+'" width="128">';
        return html;
    }
}

function Wall(response) {
    this.type = 'wall';
    var text = response.text;
    var from_id = response.from_id;
    var id = response.id;
    this.getHtml = function() {
        var html = '<div><a href="https://vk.com/wall'+from_id+'_'+id+'" target="_blank">запись на стене<a/></div>';
        return html;
    }
}

function Doc(response) {
    var owner_id = response.owner_id;
    var did = response.did;
    var title = response.title;
    var url = response.url;
    this.type = 'doc';
    this.getHtml = function() {
        var html = '<div><a href="'+url+'" target="blank">'+title+'</a></div>';
        return html;
    }
}